# Whalar Test App

This project was generated with React version 17.0.2

## Install Whalar test

First you need clone the repository:

```
$ git clone https://gonzaloCuadros@bitbucket.org/gonzaloCuadros/whalar_test.git

```

After you need install all dependencies when you stay in the project directory

```
go to app directory and exec npm install

```

## Development server

First go to api directory and run `npm start` for a dev server in fake api, later go to app directory and run `npm run dev`

Navigate to `http://localhost:8080/`.

## Build

Run `npm run build` in app directory to build the project.

## Running tests E2E

Run `npm run cypress:open` in app directory.

## Dependencies

additionally i use:

- react-hook-form
- react-icons
