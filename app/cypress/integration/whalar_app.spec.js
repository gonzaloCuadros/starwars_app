describe("WhalarTest App", () => {
  beforeEach(() => {
    cy.visit("http://localhost:8080/");
  });

  it("frontpage can be opened", () => {
    cy.contains("Jedi identifier");
  });

  it("register can be opened", () => {
    cy.get(".title-container .btn").click();
  });

  it("register work", () => {
    cy.get(".login-form input").type("jedi", { force: true });
    cy.get(".login-form button").click({ force: true });
  });

  it("load more work", () => {
    cy.visit("http://localhost:8080/jedilist");
    cy.clock();
    cy.tick(1000);
    cy.get(".section-jedi button").click();
    cy.get(".section-jedi .card").first().click();
    cy.get(".section-jedi .btn.goback-btn").click();
    cy.get(".section-jedi .card").last().click();
  });

});
