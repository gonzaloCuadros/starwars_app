import { useContext, FC } from "react";
import CharactersContext from "../context/CharactersContext";

export const useGlobalCharacter : FC = () => {
 
  return useContext(CharactersContext).characters
};
