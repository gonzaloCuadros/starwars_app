import { useEffect, useState, useContext, FC } from "react";
import CardList from "../../components/CardList";
import getCharacters from "../../services/getCharacters";
import CharactersContext from "../../context/CharactersContext";
import Spinner from "../../components/Spinner";
import "./jedis.scss";

const INITIAL_PAGE = 1;

const JediList: FC = () => {
  const [loading, setLoading] = useState(false);

  const [page, setpage] = useState(INITIAL_PAGE);
  const { characters, setCharacters } = useContext(CharactersContext);

  const handleNextPage = () => {
    setpage((prevPage) => prevPage + 1);
  };

  useEffect(() => {
    setLoading(true);
    getCharacters().then((characters) => {
      setCharacters(characters);
      setLoading(false);
    });
  }, []);

  useEffect(() => {
    if (page === INITIAL_PAGE) return;

    setLoading(true);

    getCharacters(page).then((nextCharacters) => {
      setCharacters((prevCharacters) => prevCharacters.concat(nextCharacters));
      setLoading(false);
    });
  }, [page]);

  const buttonContainer = (
    <div className="button-container">
      <button className="btn" onClick={handleNextPage}>
        Load More
      </button>
    </div>
  );

  return (
    <section className="section-jedi">
      <div className="jedi-wrap">
        <div className="title-container">
          <h2>Star Wars Characters</h2>
        </div>
        {loading ? <Spinner /> : <CardList characters={characters} />}
        {!loading ? buttonContainer : ""}
      </div>
    </section>
  );
};

export default JediList;
