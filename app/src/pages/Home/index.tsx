import { FC } from "react";
import { useModal } from "../../hooks/useModal";
import Modal from "../../components/Modal";
import Register from "../../components/Register";
import "./home.scss";

const Home: FC = () => {
  const [isOpenModal, openModal, closeModal] = useModal(false);

  return (
    <>
      <section className="section-home">
        <div className="container">
          <div className="home-wrap">
            <div className="title-container">
              <h1>
                May the <strong>force</strong> be with you
              </h1>
              <p>
                The Biggest Problem In The Universe Is...
                <br />
                That Nobody Helps Each Other.
              </p>
              <button className="btn" onClick={openModal}>
                Jedi identifier
              </button>
            </div>
            <div className="img-container"></div>
          </div>
        </div>
      </section>
      <Modal isOpen={isOpenModal} closeModal={closeModal}>
        <Register />
      </Modal>
    </>
  );
};

export default Home;
