import { FC } from "react";
import { useParams } from "react-router-dom";
import { useGlobalCharacter } from "../../hooks/useGlobalCharacter";
import { GobackBtn } from "../../components/Buttons/GoBackBtn";
import Card from "../../components/Card";
import "./detail.scss";

const Jedi: FC = () => {
  const characters = useGlobalCharacter();
  const { id } = useParams();
  const currentCharacter = characters[id - 1];

  return (
    <section className="section-jedi">
      <div className="wrapper">
        <div className="button-container-header">
          <GobackBtn />
        </div>
        <Card charInfo={currentCharacter} />
      </div>
    </section>
  );
};

export default Jedi;
