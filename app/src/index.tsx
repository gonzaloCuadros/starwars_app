import ReactDom from "react-dom";
import App from "./App";
import "./common/scss/styles.scss";

ReactDom.render(<App />, document.getElementById("root"));
