import { API_URL } from "./settings";
import { PropsCardList } from "src/components/CardList/propsCardlist.vm";

const getCharacters: (page?: number) => Promise<any[] | PropsCardList> = async (
  page = 1
) => {
  const apiUrl = `${API_URL}/people?page=${page}`;

  return fetch(apiUrl)
    .then((res) => res.json())
    .then((response) => {
      const data = response.results;
      if (Array.isArray(data)) {
        const characters = data;
        return characters;
      }
    });
};

export default getCharacters;
