const ENDPOINT = " http://localhost:3001/users";

export const registerService: (data: any) => Promise<boolean> = (data) => {
  return fetch(`${ENDPOINT}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ data }),
  }).then((res) => {
    if (!res.ok) throw new Error("Response is NOT ok");
    return true;
  });
};
