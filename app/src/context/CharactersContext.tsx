import React, { createContext, useState, FC } from "react";

const Context = createContext({});

export const CharactersContextProvider: FC = ({ children }) => {
  const [characters, setCharacters] = useState([]);

  return (
    <Context.Provider value={{ characters, setCharacters }}>
      {children}
    </Context.Provider>
  );
};

export default Context;
