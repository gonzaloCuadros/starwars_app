export interface PropsCardList {
    characters: {
      name: string;
      height: string;
      gender: string;
      mass: string;
      hairColor: string;
      eyeColor: string;
      skinColor: string;
      birthYear: string;
      films: string[];
      url: string;
      id: number | null;
    };
  }