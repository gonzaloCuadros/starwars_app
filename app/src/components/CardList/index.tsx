import { FC } from "react";
import { BrowserRouter, Link, useHistory } from "react-router-dom";
import { PropsCardList } from "./propsCardlist.vm";
import "./cardlist.scss";

const CardList: FC<PropsCardList> = ({ characters }) => {
  const history = useHistory();

  const toPage = (page) => (event) => {
    event.preventDefault();
    history.push(page);
  };

  const getIdFromUrl = (url) => {
    return url ? url.replace(/\D/g, "") : "";
  };

  return (
    <section className="card-list">
      <BrowserRouter>
        {characters.map(({ name, films, birth_year, url }) => (
          <Link
            key={name}
            className="card"
            to={{
              pathname: `/jedi/${getIdFromUrl(url)}`,
              state: {
                id: true,
              },
            }}
            onClick={toPage(`/jedi/${getIdFromUrl(url)}`)}
          >
            <div className="card-header">
              <p className="names">{name}</p>
            </div>
            <img
              alt=""
              className="logos"
              src="https://cdn-icons-png.flaticon.com/512/86/86484.png"
              data-image=""
            />
            <div className="card-footer">
              <p>Films: {films.length}</p>
              <p>Birth year: {birth_year}</p>
            </div>
          </Link>
        ))}
      </BrowserRouter>
    </section>
  );
};

export default CardList;
