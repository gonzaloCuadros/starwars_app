export interface PropsCard {
    charInfo: {
      name: string;
      birth_year: string;
      height: string;
      gender: string;
      mass: string;
      hair_color: string;
      eye_color: string;
      skin_color: string;
    };
  }