import { FC } from "react";
import { capitalize } from "../../helpers/string.helper";
import { PropsCard } from "./propsCard.vm";
import "./card.scss";

const Card: FC<PropsCard> = ({ charInfo }) => {
  return (
    <div className="profile-card js-profile-card">
      <div className="profile-card__img">
        <img
          src="https://avatarfiles.alphacoders.com/272/272931.png"
          alt="profile card"
        />
      </div>

      <div className="profile-card__cnt js-profile-cnt">
        <div className="profile-card__name">{capitalize(charInfo.name)}</div>

        <div className="profile-card-loc">
          <span className="profile-card-loc__txt">{charInfo.birth_year}</span>
        </div>

        <div className="profile-card-inf">
          <div className="profile-card-inf__item">
            <div className="profile-card-inf__title">Height:</div>
            <div className="profile-card-inf__txt">{charInfo.height}</div>
          </div>

          <div className="profile-card-inf__item">
            <div className="profile-card-inf__title">Gender:</div>
            <div className="profile-card-inf__txt">
              {capitalize(charInfo.gender)}
            </div>
          </div>

          <div className="profile-card-inf__item">
            <div className="profile-card-inf__title">Mass:</div>
            <div className="profile-card-inf__txt">{charInfo.mass}</div>
          </div>

          <div className="profile-card-inf__item">
            <div className="profile-card-inf__title">Hair color:</div>
            <div className="profile-card-inf__txt">
              {capitalize(charInfo.hair_color)}
            </div>
          </div>
          <div className="profile-card-inf__item">
            <div className="profile-card-inf__title">Eye color:</div>
            <div className="profile-card-inf__txt">
              {capitalize(charInfo.eye_color)}
            </div>
          </div>
          <div className="profile-card-inf__item">
            <div className="profile-card-inf__title">Skin color:</div>
            <div className="profile-card-inf__txt">
              {capitalize(charInfo.skin_color)}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
