import { FC } from "react";
import { useHistory } from "react-router-dom";
import { BiLeftArrowAlt } from "react-icons/bi";
import "./buttons.scss";

export const GobackBtn: FC = () => {
  const history = useHistory();

  return (
    <button
      className="btn goback-btn"
      onClick={() => {
        history.goBack();
      }}
    >
      <BiLeftArrowAlt />
      <span>Back to main list</span>
    </button>
  );
};
