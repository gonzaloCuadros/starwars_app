import { useState, FC } from "react";
import { useForm } from "react-hook-form";
import { registerService } from "../../services/registerService";
import { useHistory } from "react-router-dom";

const Register = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const history = useHistory();

  const onSubmit = (values) => {
    setIsSubmitting(true);
    registerService(values).then(() => {
      setIsSubmitting(false);
      history.push("/jedilist");
    });
  };
  return (
    <>
      <div className="login-container">
        <h1>What kind of jedi are you?</h1>
        <div className="switch-container">
          <form className="login-form" onSubmit={handleSubmit(onSubmit)}>
            <input
              placeholder="Jedi"
              {...register("username", { required: true })}
            />
            {errors.username && <span>This field is required</span>}

            <button className="btn" disabled={isSubmitting}>
              Access the jedi temple
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default Register;
