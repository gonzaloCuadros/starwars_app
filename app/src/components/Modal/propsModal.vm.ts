import { FC } from "react";

export interface PropsModal {
  children: FC;
  isOpen: boolean;
  closeModal: () => void;
}
