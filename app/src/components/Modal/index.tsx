import { FC } from "react";
import { PropsModal } from "./propsModal.vm";
import "./modal.scss";

const Modal: FC<PropsModal> = ({ children, isOpen, closeModal }) => {
  return (
    <div className={`modalBackground ${isOpen && "modalShowing-true"}`}>
      <div className="modal">
        <button className="modal-close" onClick={closeModal}>
          <p>close</p>
        </button>
        {children}
      </div>
    </div>
  );
};

export default Modal;
