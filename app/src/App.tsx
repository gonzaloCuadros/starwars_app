import { FC } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Jedi from "./pages/Detail";
import JediList from "./pages/JediList";
import { CharactersContextProvider } from "./context/CharactersContext";

const App: FC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <CharactersContextProvider>
          <Route exact path="/jedilist" component={JediList} />
          <Route exact path="/jedi/:id" component={Jedi} />
        </CharactersContextProvider>
      </Switch>
    </BrowserRouter>
  );
};

export default App;
