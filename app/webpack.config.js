const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

const rulesForTypeScript = {
  test: /\.(js|jsx|tsx|ts)$/,
  exclude: /node_modules/,
  loader: "babel-loader",
};

const rulesForStyles = {
  test: /\.(css|sass|scss)$/,
  use: ["style-loader", "css-loader", "sass-loader"],
};

const rules = [rulesForTypeScript, rulesForStyles];

module.exports = (env, argv) => {
  const { mode } = argv;
  const isProduction = mode === "production";

  return {
    entry: "./src/index.tsx",
    devtool: "inline-source-map",
    module: { rules },
    plugins: [
      new HtmlWebpackPlugin({
        template: "./src/index.html",
        favicon: "./src/favicon.png",
      }),
    ],
    resolve: {
      extensions: ["*", ".js", ".jsx", ".tsx", ".ts"],
    },
    output: {
      filename: isProduction ? "[name].[contenthash].js" : "main.js",
      path: path.resolve(__dirname, "build"),
      publicPath: "/",
    },
    devServer: {
      historyApiFallback: true,
    },
  };
};
